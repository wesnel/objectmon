package cs203.objectmon.objectdex;

import static org.junit.Assert.*;

import cs203.battlearena.objectmon.ObjectdexEntry;
import cs203.battlearena.objectmon.Objectmon;
import java.io.IOException;
import java.util.NoSuchElementException;
import org.junit.Before;
import org.junit.Test;

public class TestJsonObjectdex {
  private JsonObjectdex<String, ObjectdexEntry> jodex;
  private ObjectdexEntry oEntry1;
  private ObjectdexEntry oEntry2;

  @Before
  public void init() {
    oEntry1 = new ObjectdexEntry("one", 1, 2, 3);
    oEntry2 = new ObjectdexEntry("two", 3, 2, 1);
    jodex = new JsonObjectdex<String, ObjectdexEntry>();
    jodex.put(oEntry1.getName(), oEntry1);
    jodex.put(oEntry2.getName(), oEntry2);
  }

  @Test
  public void testSaveAndLoad() {
    JsonObjectdex<String, ObjectdexEntry> jodexLoaded = new JsonObjectdex<String, ObjectdexEntry>();
    try {
      jodex.save("jodex.objectdex");
      jodexLoaded.load("jodex.objectdex");
    } catch (IOException ex) {
      fail(ex.toString());
    }
    assertEquals(jodex.get("one").getName(), jodexLoaded.get("one").getName());
    assertEquals(jodex.get("one").getHp(), jodexLoaded.get("one").getHp());
    assertEquals(jodex.get("one").getStamina(), jodexLoaded.get("one").getStamina());
    assertEquals(jodex.get("one").getWeight(), jodexLoaded.get("one").getWeight());
    assertEquals(jodex.get("two").getName(), jodexLoaded.get("two").getName());
    assertEquals(jodex.get("two").getHp(), jodexLoaded.get("two").getHp());
    assertEquals(jodex.get("two").getStamina(), jodexLoaded.get("two").getStamina());
    assertEquals(jodex.get("two").getWeight(), jodexLoaded.get("two").getWeight());
  }

  @Test(expected = IOException.class)
  public void testSaveThrowsException() throws IOException {
    jodex.save("");
  }

  @Test(expected = IOException.class)
  public void testLoadThrowsException() throws IOException {
    jodex.load("nonexistantFile");
  }

  @Test
  public void testCreateObjectmon() {
    assertEquals(
        new Objectmon(
            oEntry1.getName(), oEntry1.getHp(), oEntry1.getStamina(), oEntry1.getWeight()),
        jodex.createObjectmon("one"));
  }

  @Test(expected = NoSuchElementException.class)
  public void testCreateObjectmonThrowsException() throws NoSuchElementException {
    jodex.createObjectmon("nope");
  }
}
