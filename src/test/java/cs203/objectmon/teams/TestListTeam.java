package cs203.objectmon.teams;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNull;

import cs203.battlearena.objectmon.Objectmon;
import java.util.List;
import java.util.StringJoiner;
import org.junit.Test;

public class TestListTeam {
  @Test
  public void testListTeam() {
    // default constructor
    ListTeam<Objectmon> a = new ListTeam<Objectmon>();
    assertTrue("maxSize should be empty", a.getMaxSize() == 0);
    assertTrue("name should be empty", a.getName() == null);

    // string constructor
    ListTeam<Objectmon> b = new ListTeam<Objectmon>("b");
    assertTrue("maxSize should be empty", b.getMaxSize() == 0);
    assertTrue("name should be 'b'", b.getName() == "b");

    // string & int constructor
    ListTeam<Objectmon> c = new ListTeam<Objectmon>("c", 6);
    assertTrue("maxSize should be 6", c.getMaxSize() == 6);
    assertTrue("name should be 'c'", c.getName() == "c");
  }

  @Test
  public void testCanFight() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 1);
    assertFalse("lt has no Objectmon", lt.canFight());
    lt.add(new Objectmon());
    assertTrue("lt has a healthy Objectmon", lt.canFight());
  }

  @Test
  public void testGetMaxSize() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 6);
    assertTrue("maxSize should be 6", lt.getMaxSize() == 6);
  }

  @Test
  public void testNextObjectmon() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 2);
    assertTrue("lt has no Objectmon", lt.nextObjectmon() == null);
    lt.add(new Objectmon("fainted", 0, 1, 1));
    assertTrue("lt has only a fainted Objectmon", lt.nextObjectmon() == null);
    lt.add(new Objectmon("healthy", 1, 1, 1));
    assertTrue("lt has a healthy Objectmon", lt.nextObjectmon().getName() == "healthy");
  }

  @Test
  public void testSetMaxSize() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>();
    lt.setMaxSize(6);
    assertTrue("maxSize should be 6", lt.getMaxSize() == 6);
  }

  @Test
  public void testToString() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 1);
    lt.add(new Objectmon("oman", 1, 2, 3));
    StringJoiner sj = new StringJoiner(",", "{", "}");
    sj.add("\"" + lt.getName() + "\"");
    sj.add(Integer.toString(lt.getMaxSize()));
    sj.add("[" + lt.get(0).toString() + "]");
    assertEquals("incorrect toString", sj.toString(), lt.toString());
  }

  @Test
  public void testGetName() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("test");
    assertEquals("name should be 'test'", "test", lt.getName());
  }

  @Test
  public void testSetName() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>();
    lt.setName("test");
    assertEquals("name should be 'test'", "test", lt.getName());
  }

  // there doesn't seem to be an obvious way to test tick.
  @Test
  public void testTick() {}

  @Test
  public void testAdd() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 1);
    assertNull("no Objectmon has been added", lt.nextObjectmon());

    // adding with just an E
    lt.add(new Objectmon("oman", 1, 1, 1));
    assertEquals("an Objectmon has been added", lt.nextObjectmon().getName(), "oman");

    // hitting max size
    assertFalse("lt is at max size", lt.add(new Objectmon()));

    lt.setMaxSize(2);
    Objectmon a = new Objectmon();

    // with both an E and an index
    lt.add(0, a);
    assertEquals("Objectmon a should be put at beginning", a, lt.get(0));

    lt.setMaxSize(3);
    Objectmon b = new Objectmon();

    // appending to the end
    lt.add(b);
    assertEquals("Objectmon b should be put at end", b, lt.get(2));
  }

  @Test
  public void testAddAll() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 4);
    ListTeam<Objectmon> other = new ListTeam<Objectmon>("other", 2);
    Objectmon a = new Objectmon();
    Objectmon b = new Objectmon();
    other.add(a);
    other.add(b);

    // with just a collection
    lt.addAll(other);
    assertTrue("all Objectmon should have been added", lt.get(1).equals(b));

    // with both a collection and an index
    lt.addAll(1, other);
    assertTrue("adding all at an index should shift elements", lt.get(3).equals(b));
  }

  @Test
  public void testClear() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 2);
    lt.add(new Objectmon());
    lt.add(new Objectmon());
    lt.clear();
    assertEquals("lt should be empty", null, lt.nextObjectmon());
  }

  @Test
  public void testContains() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 2);
    Objectmon oman = new Objectmon();
    lt.add(oman);
    assertTrue("lt should contain oman", lt.contains(oman));
  }

  @Test
  public void testContainsAll() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 2);
    ListTeam<Objectmon> other = new ListTeam<Objectmon>("other", 2);
    Objectmon a = new Objectmon();
    Objectmon b = new Objectmon();
    other.add(a);
    other.add(b);
    lt.addAll(other);
    assertTrue("both teams have the same Objectmon", lt.containsAll(other));
  }

  @Test
  public void testEquals() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 3);
    ListTeam<Objectmon> other = new ListTeam<Objectmon>("other", 2);
    Objectmon a = new Objectmon();
    Objectmon b = new Objectmon();
    Objectmon c = new Objectmon();
    other.add(a);
    other.add(b);
    lt.add(a);
    assertFalse("an Objectmon is missing in lt", lt.equals(other));
    lt.add(b);
    assertTrue("both teams have the same Objectmon", lt.equals(other));
    lt.add(c);
    assertFalse("lt is bigger", lt.equals(other));
  }

  @Test
  public void testGet() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 1);
    Objectmon a = new Objectmon();
    lt.add(a);
    assertEquals("gotten Objectmon should be equivalent to one added", a, lt.get(0));
  }

  @Test
  public void testIndexOf() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 2);
    Objectmon a = new Objectmon();
    Objectmon b = new Objectmon();
    lt.add(a);
    lt.add(b);
    assertEquals("Objectmon b is in index 1", 1, lt.indexOf(b));
  }

  // I couldn't think of a more interesting test.
  @Test
  public void testSpliterator() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>();
    assertNotNull("ListTeam should have a spliterator", lt.spliterator());
  }

  @Test
  public void testIsEmpty() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 1);
    assertTrue("lt is empty", lt.isEmpty());
    lt.add(new Objectmon());
    assertFalse("lt is not empty", lt.isEmpty());
  }

  // another unfortunately boring test.
  @Test
  public void testIterator() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>();
    assertNotNull("ListTeam should have an iterator", lt.iterator());
  }

  @Test
  public void testLastIndexOf() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 3);
    Objectmon a = new Objectmon();
    Objectmon b = new Objectmon();
    lt.add(a);
    lt.add(b);
    lt.add(a);
    assertEquals("Objectmon a has last index 2", 2, lt.lastIndexOf(a));
  }

  // I wish I could come up with something cooler than this.
  @Test
  public void testListIterator() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>();
    assertNotNull("ListTeam should have a listIterator", lt.listIterator());
    assertNotNull("ListTeam should have a listIterator", lt.listIterator(0));
  }

  @Test
  public void testRemove() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 1);
    Objectmon a = new Objectmon();
    lt.add(a);
    assertTrue("oman was added", lt.get(0) == a);

    // removing by index
    lt.remove(0);
    assertNull("oman was removed", lt.nextObjectmon());

    lt.add(a);
    assertTrue("oman was added", lt.get(0) == a);

    // removing by object
    lt.remove(a);
    assertNull("oman was removed", lt.nextObjectmon());
  }

  @Test
  public void testRemoveAll() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 2);
    ListTeam<Objectmon> other = new ListTeam<Objectmon>("other", 1);
    Objectmon a = new Objectmon("a");
    Objectmon b = new Objectmon("b");
    lt.add(a);
    lt.add(b);
    other.add(a);
    lt.removeAll(other);
    assertFalse("Objectmon a was removed", lt.contains(a));
  }

  // I don't even know what a unary operator is!
  @Test
  public void testReplaceAll() {}

  @Test
  public void testRetainAll() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 2);
    ListTeam<Objectmon> other = new ListTeam<Objectmon>("other", 1);
    Objectmon a = new Objectmon("a");
    Objectmon b = new Objectmon("b");
    lt.add(a);
    lt.add(b);
    other.add(a);
    lt.retainAll(other);
    assertFalse("b should have been removed", lt.contains(b));
  }

  @Test
  public void testSet() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 2);
    Objectmon a = new Objectmon("a");
    Objectmon b = new Objectmon("b");
    lt.add(a);
    lt.add(a);
    lt.set(1, b);
    assertEquals("index 2 should be b", b, lt.get(1));
  }

  @Test
  public void testSize() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 6);
    Objectmon a = new Objectmon("a");
    lt.add(a);
    lt.add(a);
    assertEquals("lt has two members", 2, lt.size());
  }

  // I don't have any comparable objectmon on hand.
  @Test
  public void testSort() {}

  @Test
  public void testSubList() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 5);
    Objectmon a = new Objectmon("a");
    Objectmon b = new Objectmon("b");
    Objectmon c = new Objectmon("c");
    while (lt.size() < (lt.getMaxSize() - 1)) {
      lt.add(a);
      lt.add(b);
    }
    lt.add(c);
    List<Objectmon> sl = lt.subList(4, 5);
    assertEquals("index 0 of sl should be c", c, sl.get(0));
  }

  @Test
  public void testToArray() {
    ListTeam<Objectmon> lt = new ListTeam<Objectmon>("lt", 1);
    lt.add(new Objectmon());

    // with no arguments
    assertTrue("toArray() should return an Object[]", (lt.toArray() instanceof Object[]));
    assertFalse("toArray() should return an Object[]", (lt.toArray() instanceof Objectmon[]));

    // passing in an array
    assertTrue(
        "toArray(T[] arr) should return a T[]",
        (lt.toArray(new Objectmon[1]) instanceof Objectmon[]));
  }
}
