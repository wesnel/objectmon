package cs203.objectmon.teams;

import static org.junit.Assert.*;

import cs203.battlearena.objectmon.Objectmon;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import org.junit.Before;
import org.junit.Test;

public class TestFileTeam {
  private FileTeam<Objectmon> ft1;
  private FileTeam<Objectmon> ft2;
  private FileTeam<Objectmon> ft3;
  private Objectmon oman1;
  private Objectmon oman2;
  private Objectmon oman3;
  private PrintWriter out1;
  private PrintWriter out2;

  @Before
  public void init() throws IOException {
    ft1 = new FileTeam<Objectmon>();
    ft2 = new FileTeam<Objectmon>("File Team 2", 2);
    ft3 = new FileTeam<Objectmon>("File Team 3", 1);
    oman1 = new Objectmon("Oman 1", 1, 2, 3);
    oman2 = new Objectmon("Oman 2", 3, 2, 1);
    oman3 = new Objectmon("Oman 3", 7, 7, 7);
    ft2.add(oman1);
    ft2.add(oman2);
    ft3.add(oman3);
    out1 = null;
    try {
      out1 = new PrintWriter(new FileWriter("testMalformedInput1.team"));
      out1.println("name");
      out1.println(-1);
      out1.println("oman, 1, 2, 3");
    } finally {
      if (out1 != null) {
        out1.close();
      }
    }
    out2 = null;
    try {
      out2 = new PrintWriter(new FileWriter("testMalformedInput2.team"));
      out2.println("name");
      out2.println(1);
      out2.println("oman, 1, 2, 3");
      out2.println("oman, 1, 2, 3");
    } finally {
      if (out2 != null) {
        out2.close();
      }
    }
  }

  @Test
  public void testFileTeam() {
    assertEquals("File Team 2", ft2.getName());
    assertEquals(2, ft2.getMaxSize());
    assertEquals(oman1, ft2.get(0));
    assertEquals(oman2, ft2.get(1));
  }

  @Test
  public void testLoad() {
    try {
      ft2.save("testLoad.team");
      ft1.load("testLoad.team");
      assertEquals("File Team 2", ft1.getName());
      assertEquals(2, ft1.getMaxSize());
      assertEquals(oman1, ft1.get(0));
      assertEquals(oman2, ft1.get(1));
      ft3.save("testSave.team");
      ft1.load("testSave.team");
      assertEquals("File Team 3", ft1.getName());
      assertEquals(1, ft1.getMaxSize());
      assertEquals(oman3, ft1.get(0));
    } catch (IOException ioEx) {
      fail("no exceptions should have been thrown: " + ioEx);
    }
  }

  @Test(expected = IOException.class)
  public void testLoadThrowsException1() throws IOException {
    ft1.load("nonexistantFile.txt");
  }

  @Test(expected = IOException.class)
  public void testLoadThrowsException2() throws IOException {
    ft1.load("testMalformedInput1.team");
  }

  @Test(expected = IOException.class)
  public void testLoadThrowsException3() throws IOException {
    ft1.load("testMalformedInput2.team");
  }

  @Test
  public void testSave() {
    try {
      ft2.save("testSave.team");
      ft1.load("testSave.team");
      assertEquals("File Team 2", ft1.getName());
      assertEquals(2, ft1.getMaxSize());
      assertEquals(oman1, ft1.get(0));
      assertEquals(oman2, ft1.get(1));
      ft3.save("testSave.team");
      ft1.load("testSave.team");
      assertEquals("File Team 3", ft1.getName());
      assertEquals(1, ft1.getMaxSize());
      assertEquals(oman3, ft1.get(0));
    } catch (IOException ioEx) {
      fail("no exceptions should have been thrown: " + ioEx);
    }
  }

  @Test(expected = IOException.class)
  public void testSaveThrowsException() throws IOException {
    ft1.load("nonexistantFile.txt");
  }
}
