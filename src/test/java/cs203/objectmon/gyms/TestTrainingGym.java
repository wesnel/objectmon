package cs203.objectmon.gyms;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import cs203.battlearena.objectmon.Objectmon;
import cs203.battlearena.teams.BasicTeam;
import java.io.ByteArrayInputStream;
import org.junit.Test;

public class TestTrainingGym {
  @Test
  public void testTrainingGym() {
    BasicTeam basicTeamA = new BasicTeam("basicTeamA", 6);
    BasicTeam basicTeamB = new BasicTeam("basicTeamB", 6);
    TrainingGym withConstructor = new TrainingGym(basicTeamA, basicTeamB);
    assertEquals(6, withConstructor.getTeamA().getMaxSize());
    assertEquals(6, withConstructor.getTeamB().getMaxSize());
    TrainingGym defaultConstructor = new TrainingGym();
    assertEquals(0, defaultConstructor.getTeamA().getMaxSize());
    assertEquals(0, defaultConstructor.getTeamB().getMaxSize());
  }

  @Test
  public void testConfigureFight() {
    // random configuration.
    System.setIn(
        new ByteArrayInputStream("1\nrandomTeamA\n6\n1\nrandomTeamB\n6\n1\n2\n1\n".getBytes()));
    TrainingGym randomGym = new TrainingGym();
    randomGym.configureFight();
    assertTrue("randomTeamA should have objectmon.", randomGym.canTeamAFight());
    assertTrue("randomTeamB should have objectmon.", randomGym.canTeamBFight());
    assertEquals(1, randomGym.getMaxRounds());
    assertEquals("randomTeamA", randomGym.getTeamA().getName());
    assertEquals("randomTeamB", randomGym.getTeamB().getName());
    assertEquals(6, randomGym.getTeamA().getMaxSize());
    assertEquals(6, randomGym.getTeamB().getMaxSize());

    // manual configuration.
    System.setIn(
        new ByteArrayInputStream(
            "1\nmanualTeamA\n1\n2\ntestmonA\n1\n2\n3\nmanualTeamB\n1\n2\ntestmonB\n9\n8\n7\n2\n1\n"
                .getBytes()));
    TrainingGym manualGym = new TrainingGym();
    manualGym.configureFight();
    assertTrue("manualTeamA should have objectmon.", manualGym.canTeamAFight());
    assertTrue("manualTeamB should have objectmon.", manualGym.canTeamBFight());
    assertEquals(1, manualGym.getMaxRounds());
    assertEquals("manualTeamA", manualGym.getTeamA().getName());
    assertEquals("manualTeamB", manualGym.getTeamB().getName());
    assertEquals(1, manualGym.getTeamA().getMaxSize());
    assertEquals(1, manualGym.getTeamB().getMaxSize());
    assertEquals("testmonA", manualGym.getTeamA().nextObjectmon().getName());
    assertEquals(1, manualGym.getTeamA().nextObjectmon().getHP());
    assertEquals(2, manualGym.getTeamA().nextObjectmon().getStamina());
    assertEquals(3, manualGym.getTeamA().nextObjectmon().getWeight());
    assertEquals("testmonB", manualGym.getTeamB().nextObjectmon().getName());
    assertEquals(9, manualGym.getTeamB().nextObjectmon().getHP());
    assertEquals(8, manualGym.getTeamB().nextObjectmon().getStamina());
    assertEquals(7, manualGym.getTeamB().nextObjectmon().getWeight());
  }

  @Test
  public void testBuildRandomTeams() {
    TrainingGym gym = new TrainingGym();
    gym.getTeamA().setMaxSize(1);
    assertFalse("teamA has no objectmon yet.", gym.canTeamAFight());
    gym.getTeamB().setMaxSize(3);
    assertFalse("teamB has no objectmon yet.", gym.canTeamBFight());
    gym.buildRandomTeam(gym.getTeamA());
    assertTrue("teamA should have objectmon.", gym.canTeamAFight());
    gym.buildRandomTeam(gym.getTeamB());
    assertTrue("teamB should have objectmon.", gym.canTeamBFight());
  }

  @Test
  public void testAddToTeamA() {
    TrainingGym gym = new TrainingGym();
    Objectmon objectmon = new Objectmon("test");
    assertFalse("teamA has max size zero.", gym.addToTeamA(objectmon));
    gym.getTeamA().setMaxSize(1);
    assertTrue("objectmon should be added to teamA.", gym.addToTeamA(objectmon));
  }

  @Test
  public void testAddToTeamB() {
    TrainingGym gym = new TrainingGym();
    Objectmon objectmon = new Objectmon("test");
    assertFalse("teamB has max size zero.", gym.addToTeamB(objectmon));
    gym.getTeamB().setMaxSize(1);
    assertTrue("objectmon should be added to teamB.", gym.addToTeamB(objectmon));
  }

  @Test
  public void testCanTeamAFight() {
    TrainingGym gym = new TrainingGym();
    assertFalse("teamA has no objectmon.", gym.canTeamAFight());
    gym.getTeamA().setMaxSize(1);
    Objectmon dead = new Objectmon("dead", 0, 1, 1);
    gym.addToTeamA(dead);
    assertFalse("teamA has no objectmon that can fight.", gym.canTeamAFight());
    gym.clearTeams();
    Objectmon alive = new Objectmon("alive", 1, 1, 1);
    gym.addToTeamA(alive);
    assertTrue("teamA has an objectmon that can fight.", gym.canTeamAFight());
  }

  @Test
  public void testCanTeamBFight() {
    TrainingGym gym = new TrainingGym();
    assertFalse("teamB has no objectmon.", gym.canTeamBFight());
    gym.getTeamB().setMaxSize(1);
    Objectmon dead = new Objectmon("dead", 0, 1, 1);
    gym.addToTeamB(dead);
    assertFalse("teamB has no objectmon that can fight.", gym.canTeamBFight());
    gym.clearTeams();
    Objectmon alive = new Objectmon("alive", 1, 1, 1);
    gym.addToTeamB(alive);
    assertTrue("teamB has an objectmon that can fight.", gym.canTeamBFight());
  }

  @Test
  public void testIsWinner() {
    TrainingGym gym = new TrainingGym();
    assertTrue("both teams are empty.", gym.isWinner());
    gym.getTeamA().setMaxSize(1);
    gym.getTeamB().setMaxSize(1);
    Objectmon dead = new Objectmon("dead", 0, 1, 1);
    Objectmon alive = new Objectmon("alive", 1, 1, 1);
    gym.addToTeamA(dead);
    gym.addToTeamB(dead);
    assertTrue("both teams have no objectmon that can fight.", gym.isWinner());
    gym.clearTeams();
    gym.addToTeamA(alive);
    gym.addToTeamB(dead);
    assertTrue("one team has no objectmon that can fight.", gym.isWinner());
    gym.clearTeams();
    gym.addToTeamA(alive);
    gym.addToTeamB(alive);
    assertFalse("both teams have objectmon that can fight.", gym.isWinner());
  }

  @Test
  public void testToString() {
    TrainingGym gym = new TrainingGym();
    gym.getTeamA().setMaxSize(1);
    gym.getTeamB().setMaxSize(1);
    gym.getTeamA().setName("testA");
    gym.getTeamB().setName("testB");
    Objectmon omonA = new Objectmon("omonA", 1, 2, 3);
    Objectmon omonB = new Objectmon("omonB", 9, 8, 7);
    gym.addToTeamA(omonA);
    gym.addToTeamB(omonB);
    assertEquals(
        "\n\ntestA\n{\"omonA\",1,2,3,attacks:[{\"Basic Attack\"}],statusEffects:[]}\n\n\ntestB\n{\"omonB\",9,8,7,attacks:[{\"Basic Attack\"}],statusEffects:[]}\n\n",
        gym.toString());
  }

  @Test
  public void testToStringTeam() {
    TrainingGym gym = new TrainingGym();
    gym.getTeamA().setMaxSize(1);
    gym.getTeamB().setMaxSize(1);
    gym.getTeamA().setName("testA");
    gym.getTeamB().setName("testB");
    Objectmon omonA = new Objectmon("omonA", 1, 2, 3);
    Objectmon omonB = new Objectmon("omonB", 9, 8, 7);
    gym.addToTeamA(omonA);
    gym.addToTeamB(omonB);
    assertEquals(
        "\ntestA\n{\"omonA\",1,2,3,attacks:[{\"Basic Attack\"}],statusEffects:[]}\n",
        gym.toStringTeam(gym.getTeamA()));
    assertEquals(
        "\ntestB\n{\"omonB\",9,8,7,attacks:[{\"Basic Attack\"}],statusEffects:[]}\n",
        gym.toStringTeam(gym.getTeamB()));
  }
}
