package cs203.objectmon.apps;

import cs203.objectmon.gyms.JsonGym;

public class JsonApp extends FileApp {
  public JsonApp(JsonGym gym) {
    super(gym);
  }

  public static void main(String[] args) {
    JsonApp app = new JsonApp(new JsonGym());
    app.run();
  }
}
