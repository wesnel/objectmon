package cs203.objectmon.apps;

import cs203.objectmon.gyms.TrainingGym;

public class TrainingApp {
  private TrainingGym gym;

  public TrainingApp(TrainingGym gym) {
    this.setGym(gym);
  }

  public void run() {
    this.gym.configureFight();
    this.gym.fight();
  }

  public TrainingGym getGym() {
    return this.gym;
  }

  public void setGym(TrainingGym gym) {
    this.gym = gym;
  }

  public static void main(String[] args) {
    TrainingApp app = new TrainingApp(new TrainingGym());
    app.run();
  }
}
