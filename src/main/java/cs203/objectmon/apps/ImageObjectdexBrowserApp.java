package cs203.objectmon.apps;

import cs203.objectmon.objectdex.view.ImageObjectdexBrowser;
import java.util.Random;

public class ImageObjectdexBrowserApp extends ObjectdexBrowserApp {
  public ImageObjectdexBrowserApp(ImageObjectdexBrowser objectdexBrowser) {
    super(objectdexBrowser);
  }

  public static void main(String[] args) {
    ImageObjectdexBrowserApp app = new ImageObjectdexBrowserApp(
        new ImageObjectdexBrowser("Image Objectdex", new Random()));
    app.run();
  }
}
