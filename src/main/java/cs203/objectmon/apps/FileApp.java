package cs203.objectmon.apps;

import cs203.battlearena.objectmon.Objectmon;
import cs203.objectmon.gyms.FileGym;
import cs203.objectmon.teams.FileTeam;
import java.io.IOException;
import java.util.Scanner;

public class FileApp extends TrainingApp {
  public FileApp(FileGym gym) {
    super(gym);
  }

  @Override
  public void run() {
    super.run();

    Scanner scanner = new Scanner(System.in);

    System.out.println("Do you want to save your teams? (1: yes / 2: no)");

    int saveChoice = scanner.nextInt();

    scanner.nextLine();

    if (saveChoice != 1) {
      // we don't save the teams.
      return;
    } else {
      // saving team A.
      FileTeam<Objectmon> teamA =
          new FileTeam<Objectmon>(
              this.getGym().getTeamA().getName(), this.getGym().getTeamA().getMaxSize());

      teamA.addAll(this.getGym().getTeamA());
      System.out.println("Enter the path at which to save team A: ");

      try {
        teamA.save(scanner.nextLine());
      } catch (IOException e) {
        e.printStackTrace();
      }

      // saving team B.
      FileTeam<Objectmon> teamB =
          new FileTeam<Objectmon>(
              this.getGym().getTeamB().getName(), this.getGym().getTeamB().getMaxSize());

      teamB.addAll(this.getGym().getTeamB());
      System.out.println("Enter the path at which to save team B: ");

      try {
        teamB.save(scanner.nextLine());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    FileApp app = new FileApp(new FileGym());
    app.run();
  }
}
