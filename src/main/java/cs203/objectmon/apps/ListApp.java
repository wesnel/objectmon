package cs203.objectmon.apps;

import cs203.objectmon.gyms.ListGym;

public class ListApp extends TrainingApp {
  public ListApp(ListGym gym) {
    super(gym);
  }

  public static void main(String[] args) {
    ListApp app = new ListApp(new ListGym());
    app.run();
  }
}
