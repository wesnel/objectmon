package cs203.objectmon.apps;

import cs203.objectmon.objectdex.view.ObjectdexBrowser;

public class ObjectdexBrowserApp {
  private ObjectdexBrowser objectdexBrowser;

  public ObjectdexBrowserApp(ObjectdexBrowser objectdexBrowser) {
    this.objectdexBrowser = objectdexBrowser;
  }

  public void run() {
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        try {
          objectdexBrowser.createAndShowGUI();
        } catch (Exception e) {
          e.printStackTrace();
          System.exit(1);
        }
      }
    });
  }

  public static void main(String[] args) {
    ObjectdexBrowserApp app =
        new ObjectdexBrowserApp(new ObjectdexBrowser("Objectdex"));
    app.run();
  }
}
