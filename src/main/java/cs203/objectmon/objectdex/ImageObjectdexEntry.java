package cs203.objectmon.objectdex;

import cs203.battlearena.objectmon.ObjectdexEntry;

public class ImageObjectdexEntry extends ObjectdexEntry {
  private String imagePath;

  public ImageObjectdexEntry(String name, int hp, int stamina, int weight)
      throws Exception {
    super(name, hp, stamina, weight);

    this.imagePath = "no-image.png";
  }

  public ImageObjectdexEntry(String name, int hp, int stamina, int weight,
                             String imagePath) {
    super(name, hp, stamina, weight);

    this.imagePath = imagePath;
  }

  public String getImagePath() { return this.imagePath; }

  public void setImagePath(String imagePath) { this.imagePath = imagePath; }
}
