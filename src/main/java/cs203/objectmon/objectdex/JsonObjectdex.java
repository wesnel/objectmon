package cs203.objectmon.objectdex;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import cs203.battlearena.objectmon.Objectdex;
import cs203.battlearena.objectmon.ObjectdexEntry;
import cs203.battlearena.objectmon.Objectmon;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.NoSuchElementException;

public class JsonObjectdex<K extends String, V extends ObjectdexEntry>
    extends HashMap<String, ObjectdexEntry> implements Objectdex, Serializable {
  private static final long serialVersionUID = 1L;

  public JsonObjectdex() {
    super();
  }

  /**
   * Creates a new Objectmon created from the ObjectdexEntry value associated with the key given by
   * the "name" parameter.
   *
   * @param name the key of the ObjectdexEntry to be used for the creation of the Objectmon.
   * @throws NoSuchElementException if "name" is not a valid key.
   */
  public Objectmon createObjectmon(String name) throws NoSuchElementException {
    if (!this.containsKey(name)) {
      throw new NoSuchElementException(name);
    }

    ObjectdexEntry oman = this.get(name);

    return new Objectmon(oman.getName(), oman.getHp(), oman.getStamina(), oman.getWeight());
  }

  /**
   * Clears this JsonObjectdex and then fills it with new ObjectdexEntry objects found in the JSON
   * file at the given path.
   *
   * @param path the path to the JSON file containing ObjectdexEntry objects.
   * @throws IOException if there is an error with reading from file.
   */
  public void load(String path) throws IOException {
    this.clear();

    BufferedReader in = null;
    Gson gson = new Gson();

    try {
      in = new BufferedReader(new FileReader(path));
      Type inputType = new TypeToken<Collection<HashMap<String, String>>>() {}.getType();

      String line;
      while ((line = in.readLine()) != null) {
        ArrayList<HashMap<String, String>> data = gson.fromJson(line, inputType);

        for (HashMap<String, String> oman : data) {
          this.put(oman.get("name"), this.buildObjectdexEntry(oman));
        }
      }
    } finally {
      if (in != null) {
        in.close();
      }
    }
  }

  public ObjectdexEntry buildObjectdexEntry(HashMap<String, String> oman) {
    return new ObjectdexEntry(
        oman.get("name"),
        Integer.parseInt(oman.get("hp")),
        Integer.parseInt(oman.get("stamina")),
        Integer.parseInt(oman.get("weight")));
  }

  /**
   * Writes the values of this JsonObjectdex to a file at the given path, using the JSON format.
   *
   * @param path the path at which to write the JSON file.
   * @throws IOException if there is an error with writing to file.
   */
  public void save(String path) throws IOException {
    Gson gson = new Gson();
    PrintWriter out = null;
    ArrayList<ObjectdexEntry> values = new ArrayList<ObjectdexEntry>(this.values());

    try {
      out = new PrintWriter(new FileWriter(path));

      out.println(gson.toJson(values));
    } finally {
      if (out != null) {
        out.close();
      }
    }
  }
}
