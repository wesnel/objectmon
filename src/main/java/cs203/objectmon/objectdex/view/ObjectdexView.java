package cs203.objectmon.objectdex.view;

import cs203.battlearena.objectmon.Objectdex;
import cs203.battlearena.objectmon.ObjectdexEntry;
import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;
import javax.swing.*;

public class ObjectdexView extends JPanel implements Serializable {
  private static final long serialVersionUID = 1L;
  private Objectdex objectdex;
  private JPanel cards;

  public ObjectdexView(Objectdex objectdex) {
    super(new BorderLayout());
    this.objectdex = objectdex;
  }

  public Objectdex getObjectdex() {
    return this.objectdex;
  }

  public void setObjectdex(Objectdex objectdex) {
    this.objectdex = objectdex;
  }

  public void createAndAddComponents() throws Exception {
    this.createAndAddCards();
    this.createAndAddNavigateButtons();
  }

  public void createAndAddCards() throws Exception {
    this.cards = this.createCards();
    this.add(this.cards, BorderLayout.CENTER);
  }

  public JPanel createCards() throws Exception {
    JPanel cards = new JPanel(new CardLayout());

    for (ObjectdexEntry ode : this.objectdex.values()) {
      ObjectdexEntryView odev = this.createObjectdexEntryView(ode);

      cards.add(odev, ode.getName());
    }

    return cards;
  }

  public void buildObjectdexView() throws Exception {
    this.createAndAddComponents();
  }

  public ObjectdexEntryView createObjectdexEntryView(ObjectdexEntry entry) throws Exception {
    ObjectdexEntryView oentryView = new ObjectdexEntryView(entry);
    return oentryView;
  }

  public void createAndAddNavigateButtons() throws Exception {
    this.createAndAddPreviousButton(this.createImageIcon("left-arrow-small.png", "previous"));
    this.createAndAddNextButton(this.createImageIcon("right-arrow-small.png", "next"));
  }

  public ImageIcon createImageIcon(String resourceName, String altText) throws Exception {
    java.net.URL imageURL = ObjectdexView.class.getClassLoader().getResource(resourceName);

    if (imageURL != null) {
      ImageIcon imageIcon = new ImageIcon(imageURL, altText);

      return imageIcon;
    } else {
      throw new Exception("ObjectdexView image not found: " + resourceName);
    }
  }

  public void createAndAddPreviousButton(ImageIcon imageIcon) {
    JButton buttonPrevious = new JButton(imageIcon);
    JPanel cards = this.cards;

    buttonPrevious.addActionListener(
        new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            CardLayout cdl = (CardLayout) (cards.getLayout());

            cdl.previous(cards);
          }
        });
    this.add(buttonPrevious, BorderLayout.LINE_START);
  }

  public void createAndAddNextButton(ImageIcon imageIcon) {
    JButton buttonNext = new JButton(imageIcon);
    JPanel cards = this.cards;

    buttonNext.addActionListener(
        new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            CardLayout cdl = (CardLayout) (cards.getLayout());

            cdl.next(cards);
          }
        });
    this.add(buttonNext, BorderLayout.LINE_END);
  }
}
