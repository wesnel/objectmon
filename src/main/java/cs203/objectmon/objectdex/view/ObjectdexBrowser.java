package cs203.objectmon.objectdex.view;

import cs203.battlearena.ObjectmonNameGenerator;
import cs203.battlearena.objectmon.Objectdex;
import cs203.battlearena.objectmon.ObjectdexEntry;
import cs203.battlearena.objectmon.Objectmon;
import cs203.objectmon.objectdex.JsonObjectdex;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.Serializable;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ObjectdexBrowser extends JFrame implements Serializable {
  private static final long serialVersionUID = 1L;
  private Objectdex objectdex;
  private ObjectdexView objectdexView;
  private JMenuBar menuBar;

  public ObjectdexBrowser(String title) {
    super(title);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  public void setObjectdex(Objectdex objectdex) throws Exception {
    this.objectdex = objectdex;

    if (this.objectdexView != null) {
      this.remove(this.objectdexView);
    }

    this.objectdexView = this.createObjectdexView(this.objectdex);

    this.getContentPane().add(this.objectdexView, BorderLayout.CENTER);
    this.pack();
    this.validate();
    this.repaint();
  }

  protected void createAndAddMenuBar() throws Exception {
    this.menuBar = new JMenuBar();

    this.createAndAddFileMenu(this.menuBar);
    this.setJMenuBar(this.menuBar);
  }

  protected void createAndAddFileMenu(JMenuBar menuBar) {
    JMenu fileMenu = new JMenu("File");

    this.createAndAddNewObjectdexMenuItem(fileMenu);
    this.createAndAddOpenObjectdexMenuItem(fileMenu);
    this.createAndAddSaveObjectdexMenuItem(fileMenu);

    menuBar.add(fileMenu);
  }

  protected void createAndAddNewObjectdexMenuItem(JMenu menu) {
    JMenuItem newMenuItem = new JMenuItem("New Objectdex");
    JFrame frame = this;

    newMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        try {
          String number = JOptionPane.showInputDialog(
              frame, "How many Objectmon to generate?", 5);
          int numToGen = Integer.parseInt(number);
          objectdex = createObjectdex(numToGen);
        } catch (NumberFormatException ex) {
          ex.printStackTrace();
          JOptionPane.showMessageDialog(frame, ex.toString());
          return;
        } catch (Exception ex) {
          ex.printStackTrace();
          JOptionPane.showMessageDialog(frame, ex.toString());
          return;
        }

        try {
          setObjectdex(objectdex);
        } catch (Exception ex) {
          ex.printStackTrace();
          JOptionPane.showMessageDialog(frame, ex.toString());
          return;
        }
      }
    });

    menu.add(newMenuItem);
  }

  public void createAndAddOpenObjectdexMenuItem(JMenu menu) {
    JMenuItem openMenuItem = new JMenuItem("Open Objectdex");
    JFrame frame = this;

    openMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String cwd = System.getProperty("user.dir");
        JFileChooser fc = new JFileChooser(cwd);
        FileNameExtensionFilter extFilter = new FileNameExtensionFilter(
            "OBJECTDEX FILES", "objectdex", "objectdex");

        fc.setFileFilter(extFilter);

        int retVal = fc.showOpenDialog(frame);

        if (retVal == JFileChooser.APPROVE_OPTION) {
          File file = fc.getSelectedFile();
          String path = file.getPath();

          try {
            loadObjectdex(path);
          } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(frame, ex.toString());
          }
        }
      }
    });

    menu.add(openMenuItem);
  }

  public void createAndAddSaveObjectdexMenuItem(JMenu menu) {
    JMenuItem saveMenuItem = new JMenuItem("Save Objectdex");
    JFrame frame = this;

    saveMenuItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String cwd = System.getProperty("user.dir");
        JFileChooser fc = new JFileChooser(cwd);
        FileNameExtensionFilter extFilter = new FileNameExtensionFilter(
            "OBJECTDEX FILES", "objectdex", "objectdex");

        fc.setFileFilter(extFilter);

        int retVal = fc.showSaveDialog(frame);

        if (retVal == JFileChooser.APPROVE_OPTION) {
          File file = fc.getSelectedFile();
          String path = file.getPath();

          try {
            objectdex.save(path);
          } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(frame, ex.toString());
          }
        }
      }
    });

    menu.add(saveMenuItem);
  }

  public void createAndShowGUI() throws Exception {
    this.createAndAddMenuBar();
    this.setSize(400, 300);
    this.setLocationRelativeTo(null);
    this.setVisible(true);
  }

  public ObjectdexView createObjectdexView(Objectdex objectdex)
      throws Exception {
    ObjectdexView odexView = new ObjectdexView(objectdex);

    odexView.buildObjectdexView();
    return odexView;
  }

  public Objectdex createObjectdex(int numEntries) throws Exception {
    if (numEntries < 0) {
      throw new Exception(
          "Can't generate an Objectdex with less than 1 entry!");
    }

    Objectdex od = new JsonObjectdex<String, ObjectdexEntry>();

    for (int i = 0; i < numEntries; i++) {
      Objectmon omon = new Objectmon(ObjectmonNameGenerator.nextName());

      od.put(omon.getName(),
             new ObjectdexEntry(omon.getName(), omon.getHp(), omon.getStamina(),
                                omon.getWeight()));
    }

    return od;
  }

  public void loadObjectdex(String objectdexPath) throws Exception {
    objectdex = new JsonObjectdex<String, ObjectdexEntry>();

    objectdex.load(objectdexPath);
    this.setObjectdex(objectdex);
  }
}
