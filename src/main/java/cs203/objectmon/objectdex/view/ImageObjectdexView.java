package cs203.objectmon.objectdex.view;

import cs203.battlearena.objectmon.Objectdex;
import cs203.battlearena.objectmon.ObjectdexEntry;
import java.io.Serializable;

public class ImageObjectdexView extends ObjectdexView implements Serializable {
  private static final long serialVersionUID = 1L;

  public ImageObjectdexView(Objectdex objectdex) throws Exception {
    super(objectdex);
  }

  @Override
  public ObjectdexEntryView createObjectdexEntryView(ObjectdexEntry entry) throws Exception {
    ImageObjectdexEntryView oentryView = new ImageObjectdexEntryView(entry);
    return oentryView;
  }
}
