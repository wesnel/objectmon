package cs203.objectmon.objectdex.view;

import cs203.battlearena.objectmon.ObjectdexEntry;
import cs203.objectmon.objectdex.ImageObjectdexEntry;
import java.io.Serializable;
import javax.swing.ImageIcon;

public class ImageObjectdexEntryView
    extends ObjectdexEntryView implements Serializable {
  private static final long serialVersionUID = 1L;

  public ImageObjectdexEntryView(ObjectdexEntry entry) throws Exception {
    super(entry);
  }

  @Override
  public ImageIcon getImageIcon() throws Exception {
    ObjectdexEntry entry = this.getEntry();

    if (entry instanceof ImageObjectdexEntry) {
      return this.getImageIconFromResource(
          ((ImageObjectdexEntry)entry).getImagePath(), entry.getName());
    } else {
      return super.getImageIcon();
    }
  }
}
