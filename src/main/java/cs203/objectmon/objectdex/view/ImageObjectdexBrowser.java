package cs203.objectmon.objectdex.view;

import cs203.battlearena.ObjectmonNameGenerator;
import cs203.battlearena.objectmon.Objectdex;
import cs203.battlearena.objectmon.ObjectdexEntry;
import cs203.battlearena.objectmon.Objectmon;
import cs203.objectmon.objectdex.ImageJsonObjectdex;
import cs203.objectmon.objectdex.ImageObjectdexEntry;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.*;

public class ImageObjectdexBrowser
    extends ObjectdexBrowser implements Serializable {
  private static final long serialVersionUID = 1L;
  private Random rng;

  public ImageObjectdexBrowser(String title, Random rng) {
    super(title);
    this.rng = rng;
  }

  @Override
  public ObjectdexView createObjectdexView(Objectdex objectdex)
      throws Exception {
    ImageObjectdexView odexView = new ImageObjectdexView(objectdex);

    odexView.buildObjectdexView();
    return odexView;
  }

  @Override
  public Objectdex createObjectdex(int numEntries) throws Exception {
    if (numEntries < 0) {
      throw new Exception(
          "Can't generate an Objectdex with less than 1 entry!");
    }

    ImageJsonObjectdex od =
        new ImageJsonObjectdex<String, ImageObjectdexEntry>();

    ArrayList<String> iconFiles = null;
    try {
      iconFiles = this.getAvailableObjectmonImages();
    } catch (IOException e) {
      iconFiles = new ArrayList<String>();
    }

    for (int i = 0; i < numEntries; i++) {
      Objectmon omon = new Objectmon(ObjectmonNameGenerator.nextName());

      od.put(omon.getName(),
             new ImageObjectdexEntry(omon.getName(), omon.getHp(),
                                     omon.getStamina(), omon.getWeight(),
                                     this.getRandomObjectmonImage(iconFiles)));
    }

    return od;
  }

  @Override
  public void loadObjectdex(String objectdexPath) throws Exception {
    ImageJsonObjectdex objectdex =
        new ImageJsonObjectdex<String, ImageObjectdexEntry>();

    objectdex.load(objectdexPath);
    this.setObjectdex(objectdex);
  }

  public ArrayList<String> getAvailableObjectmonImages() throws IOException {
    BufferedReader in = null;
    ArrayList<String> availableImages = new ArrayList<String>();

    try {
      in = new BufferedReader(
          new FileReader(ImageObjectdexBrowser.class.getClassLoader()
                             .getResource("available_images.txt")
                             .getPath()));
      String line;

      while ((line = in.readLine()) != null) {
        availableImages.add(line);
      }
    } finally {
      if (in != null) {
        in.close();
      }
    }

    return availableImages;
  }

  public String getRandomObjectmonImage(ArrayList<String> availableImages) {
    if (0 < availableImages.size()) {
      return availableImages.get(this.rng.nextInt(availableImages.size()));
    } else {
      return "no-image.png";
    }
  }
}
