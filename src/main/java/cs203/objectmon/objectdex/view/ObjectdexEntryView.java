package cs203.objectmon.objectdex.view;

import cs203.battlearena.gui.AbstractObjectmonView;
import cs203.battlearena.objectmon.ObjectdexEntry;
import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;
import javax.swing.*;

public class ObjectdexEntryView
    extends AbstractObjectmonView implements Serializable {
  private static final long serialVersionUID = 1L;
  private JLabel labelName;
  private JLabel labelHp;
  private JLabel labelStamina;
  private JLabel labelWeight;
  private JLabel labelImage;
  private ObjectdexEntry entry;

  public ObjectdexEntryView(ObjectdexEntry entry) throws Exception {
    super();

    this.entry = entry;

    this.createAndAddComponents();
  }

  public JPanel createStatsPanel() throws Exception {
    JPanel statsPanel = new JPanel();
    BoxLayout statsLayout = new BoxLayout(statsPanel, BoxLayout.Y_AXIS);

    statsPanel.setLayout(statsLayout);

    this.labelName = new JLabel("Name: " + this.getName());

    statsPanel.add(this.labelName);

    this.labelHp = new JLabel("HP: " + Integer.toString(this.getHp()));

    statsPanel.add(this.labelHp);

    this.labelStamina =
        new JLabel("Stamina: " + Integer.toString(this.getStamina()));

    statsPanel.add(this.labelStamina);

    this.labelWeight =
        new JLabel("Weight: " + Integer.toString(this.getWeight()));

    statsPanel.add(this.labelWeight);
    return statsPanel;
  }

  /** Creates and adds all sub-components */
  public void createAndAddComponents() throws Exception {
    this.createAndAddStatsPanel();
    this.createAndAddImage();
  }

  /** Creates and adds the image component to this component. */
  protected void createAndAddImage() throws Exception {
    this.addImage(this.createImage());
  }

  /** Adds the image to this component */
  protected void addImage(JLabel labelImage) throws Exception {
    this.add(labelImage);
  }

  /**
   * Creates the component that is used as the image of the {@code
   * ObjectdexEntry}.
   *
   * @return A JLabel that will be used as the image component of the entry.
   */
  protected JLabel createImage() throws Exception {
    labelImage = new JLabel(this.getImageIcon(), JLabel.CENTER);
    return labelImage;
  }

  /** Creates and adds a component to contain stats related information. */
  protected void createAndAddStatsPanel() throws Exception {
    this.addStatsPanel(this.createStatsPanel());
  }

  /** Adds the stats panel to this component */
  protected void addStatsPanel(JPanel statsPanel) { this.add(statsPanel); }

  protected ObjectdexEntry getEntry() { return this.entry; }

  public String getName() { return this.entry.getName(); }

  public int getHp() { return this.entry.getHp(); }

  public int getStamina() { return this.entry.getStamina(); }

  public int getWeight() { return this.entry.getWeight(); }

  public ImageIcon getImageIcon() throws Exception {
    return this.getImageIconFromResource("no-image.png", "default text");
  }

  public ImageIcon getImageIconFromResource(String resourceName, String altText)
      throws Exception {
    java.net.URL imageURL =
        ObjectdexEntryView.class.getClassLoader().getResource(resourceName);

    if (imageURL != null) {
      ImageIcon imageIcon = new ImageIcon(imageURL, altText);

      return imageIcon;
    } else {
      throw new Exception("ObjectdexEntryView image path not found: " +
                          resourceName);
    }
  }
}
