package cs203.objectmon.objectdex;

import cs203.battlearena.objectmon.ObjectdexEntry;
import java.util.HashMap;

public class ImageJsonObjectdex<K extends String, V extends ObjectdexEntry>
    extends JsonObjectdex<K, V> {
  @Override
  public ObjectdexEntry buildObjectdexEntry(HashMap<String, String> oman) {
    return new ImageObjectdexEntry(
        oman.get("name"),
        Integer.parseInt(oman.get("hp")),
        Integer.parseInt(oman.get("stamina")),
        Integer.parseInt(oman.get("weight")),
        oman.getOrDefault("imagePath", "no-image.png"));
  }
}
