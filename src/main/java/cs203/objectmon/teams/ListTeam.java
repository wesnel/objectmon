package cs203.objectmon.teams;

import cs203.battlearena.objectmon.Objectmon;
import cs203.battlearena.teams.Team;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.StringJoiner;
import java.util.function.UnaryOperator;

public class ListTeam<E extends Objectmon> implements Team<E>, List<E> {
  private ArrayList<E> backingArray;
  private String name;
  private int maxSize;

  /** Default constructor. Gives the team a null name and a max size of 0. */
  public ListTeam() {
    this.setName(null);
    this.setMaxSize(0);
    this.backingArray = new ArrayList<E>(this.getMaxSize());
  }

  /**
   * Constructor that takes in a name. Gives the team this name and a max size of 0.
   *
   * @param name the name for this ListTeam.
   */
  public ListTeam(String name) {
    this.setName(name);
    this.setMaxSize(0);
    this.backingArray = new ArrayList<E>(this.getMaxSize());
  }

  /**
   * Constructor that takes in both a name and a value for max size. Gives the team this name and
   * max size.
   *
   * @param name the name for this ListTeam.
   * @param maxSize the max size for this ListTeam.
   */
  public ListTeam(String name, int maxSize) {
    this.setName(name);
    this.setMaxSize(maxSize);
    this.backingArray = new ArrayList<E>(this.getMaxSize());
  }

  public boolean add(E oman) {
    if (this.size() >= this.getMaxSize()) {
      return false;
    } else {
      return this.backingArray.add(oman);
    }
  }

  public void add(int index, E oman) {
    if (this.size() >= this.getMaxSize()) {
      return;
    } else {
      this.backingArray.add(index, oman);
    }
  }

  public boolean addAll(Collection<? extends E> c) {
    if (c.size() > (this.getMaxSize() - this.size())) {
      return false;
    } else {
      return this.backingArray.addAll(c);
    }
  }

  public boolean addAll(int index, Collection<? extends E> c) {
    if (c.size() > (this.getMaxSize() - this.size())) {
      return false;
    } else {
      return this.backingArray.addAll(index, c);
    }
  }

  public void clear() {
    this.backingArray.clear();
  }

  public boolean contains(Object obj) {
    return this.backingArray.contains(obj);
  }

  public boolean containsAll(Collection<? extends Object> c) {
    return this.backingArray.containsAll(c);
  }

  public boolean canFight() {
    if (this.nextObjectmon() == null) {
      return false;
    } else {
      return true;
    }
  }

  public boolean equals(ListTeam<E> lt) {
    if (this.size() != lt.size()) {
      return false;
    } else {
      for (int i = 0; i < this.size(); i++) {
        if (this.get(i) != lt.get(i)) {
          return false;
        }
      }
      return true;
    }
  }

  public int getMaxSize() {
    return this.maxSize;
  }

  public String getName() {
    return this.name;
  }

  public E get(int index) {
    return this.backingArray.get(index);
  }

  public int indexOf(Object obj) {
    return this.backingArray.indexOf(obj);
  }

  public boolean isEmpty() {
    return this.backingArray.isEmpty();
  }

  public Iterator<E> iterator() {
    return this.backingArray.iterator();
  }

  public int lastIndexOf(Object obj) {
    return this.backingArray.lastIndexOf(obj);
  }

  public ListIterator<E> listIterator() {
    return this.backingArray.listIterator();
  }

  public ListIterator<E> listIterator(int index) {
    return this.backingArray.listIterator(index);
  }

  public E nextObjectmon() {
    ListIterator<E> li = this.listIterator();
    while (li.hasNext()) {
      E oman = li.next();
      if (!oman.isFainted()) {
        return oman;
      }
    }
    return null;
  }

  public E remove(int index) {
    return this.backingArray.remove(index);
  }

  public boolean remove(Object obj) {
    return this.backingArray.remove(obj);
  }

  public boolean removeAll(Collection<? extends Object> c) {
    return this.backingArray.removeAll(c);
  }

  public void replaceAll(UnaryOperator<E> operator) {
    this.backingArray.replaceAll(operator);
  }

  public boolean retainAll(Collection<? extends Object> c) {
    return this.backingArray.retainAll(c);
  }

  public E set(int index, E oman) {
    return this.backingArray.set(index, oman);
  }

  public void setName(String name) {
    this.name = name;
  }

  public int size() {
    return this.backingArray.size();
  }

  public void sort(Comparator<? super E> c) {
    this.backingArray.sort(c);
  }

  public Spliterator<E> spliterator() {
    return this.backingArray.spliterator();
  }

  public List<E> subList(int fromIndex, int toIndex) {
    return this.backingArray.subList(fromIndex, toIndex);
  }

  public void setMaxSize(int n) {
    this.maxSize = n;
  }

  public String toString() {
    StringJoiner sj = new StringJoiner(",", "{", "}");
    sj.add("\"" + this.getName() + "\"");
    sj.add(Integer.toString(this.getMaxSize()));
    sj.add(this.backingArray.toString());
    return sj.toString();
  }

  public void tick() {
    ListIterator<E> li = this.listIterator();
    while (li.hasNext()) {
      li.next().tick();
    }
  }

  public Object[] toArray() {
    return this.backingArray.toArray();
  }

  public <T> T[] toArray(T[] arr) {
    return this.backingArray.toArray(arr);
  }
}
