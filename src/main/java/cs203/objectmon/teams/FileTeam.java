package cs203.objectmon.teams;

import cs203.battlearena.io.Loadable;
import cs203.battlearena.io.Saveable;
import cs203.battlearena.objectmon.Objectmon;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringJoiner;

public class FileTeam<E extends Objectmon> extends ListTeam<E> implements Saveable, Loadable {

  /** Default constructor. Gives the team a null name and a max size of 0. */
  public FileTeam() {
    super();
  }

  /**
   * Constructor that takes in a name. Gives the team this name and a max size of 0.
   *
   * @param name the name for this FileTeam.
   */
  public FileTeam(String name) {
    super(name);
  }

  /**
   * Constructor that takes in both a name and a value for max size. Gives the team this name and
   * max size.
   *
   * @param name the name for this FileTeam.
   * @param maxSize the max size for this FileTeam.
   */
  public FileTeam(String name, int maxSize) {
    super(name, maxSize);
  }

  /**
   * Clears the current team and replaces it with team data loaded from the given path.
   *
   * @param path the path to the file containing the team data.
   * @throws IOException if file reading fails or there is malformed data.
   */
  public void load(String path) throws IOException {
    // clear the current team data, since it's about to be replaced.
    this.clear();

    BufferedReader in = null;
    try {

      in = new BufferedReader(new FileReader(path));

      // name should be on the first line.
      String firstLine = in.readLine();
      if (firstLine == null) {
        // there is no first line.
        throw new IOException("line was null: " + firstLine);
      }
      this.setName(firstLine);

      // max size should be on the second line.
      String secondLine = in.readLine();
      if (secondLine == null) {
        // there is no second line.
        throw new IOException("line was null: " + secondLine);
      }
      try {
        Integer maxSize = Integer.parseInt(secondLine);
        if (maxSize < 0) {
          // max size is not a nonnegative number.
          throw new IOException("invalid team size: " + secondLine);
        }
        this.setMaxSize(maxSize);
      } catch (NumberFormatException numEx) {
        // string cannot be interpreted as an integer.
        throw new IOException("error while parsing number: " + secondLine);
      }

      // reading an arbitrary number of remaining lines to get objectmon data.
      String line;
      while ((line = in.readLine()) != null) {
        try {

          String[] lineData = line.split(", ");
          if (lineData.length != 4) {
            // not enough data was provided to make an objectmon.
            throw new IOException("incorrect number of parameters: " + line);
          }

          String name = lineData[0];
          Integer hp = Integer.parseInt(lineData[1]);
          Integer stamina = Integer.parseInt(lineData[2]);
          Integer weight = Integer.parseInt(lineData[3]);

          Objectmon oman = new Objectmon(name, hp, stamina, weight);
          boolean added = this.add((E) oman);
          if (!added) {
            // for whatever reason, the objectmon was not added.
            throw new IOException("objectmon could not be added to team: " + line);
          }
        } catch (NumberFormatException numEx) {
          // exception if the string cannot be interpreted as an integer.
          throw new IOException("error while parsing number: " + line);
        }
      }
    } finally {
      // closing the file.
      if (in != null) {
        in.close();
      }
    }
  }

  /**
   * Saves this team's data to a file at the specified path.
   *
   * @param path the path to the file where this team's data will be saved.
   * @throws IOException if there is an error with writing to file.
   */
  public void save(String path) throws IOException {
    PrintWriter out = null;
    try {
      out = new PrintWriter(new FileWriter(path));

      // printing the name.
      out.println(this.getName());

      // printing the max size.
      out.println(this.getMaxSize());

      for (int i = 0; i < this.size(); i++) {
        // gathering data from each objectmon.
        StringJoiner sj = new StringJoiner(", ");
        Objectmon oman = this.get(i);
        sj.add(oman.getName());
        sj.add(new Integer(oman.getHP()).toString());
        sj.add(new Integer(oman.getStamina()).toString());
        sj.add(new Integer(oman.getWeight()).toString());

        // printing the objectmon data.
        out.println(sj.toString());
      }
    } finally {
      // closing the file.
      if (out != null) {
        out.close();
      }
    }
  }
}
