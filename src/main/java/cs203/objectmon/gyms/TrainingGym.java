package cs203.objectmon.gyms;

import cs203.battlearena.AbstractGym;
import cs203.battlearena.ObjectmonNameGenerator;
import cs203.battlearena.objectmon.Objectmon;
import cs203.battlearena.teams.Team;
import java.util.Scanner;
import java.util.StringJoiner;

public class TrainingGym extends AbstractGym {
  private Scanner scanner;

  /**
   * Constructor that takes in two Teams.
   *
   * @param teamA one of the two teams being passed in.
   * @param teamB the other team being passed in.
   */
  public TrainingGym(Team teamA, Team teamB) {
    super(teamA, teamB);

    this.scanner = new Scanner(System.in);
  }

  /**
   * Default constructor that creates two BasicTeams named teamA and teamB, which are initialized to
   * have a maximum size of zero. It is recommended to run configureFight to modify this.
   */
  public TrainingGym() {
    super();

    this.scanner = new Scanner(System.in);
  }

  public Scanner getScanner() {
    return this.scanner;
  }

  public void setScanner(Scanner scanner) {
    this.scanner = scanner;
  }

  public void configureTeam(Team team) {
    // set the team name.
    System.out.println("Enter the name for this team:");

    String name = this.scanner.next();

    team.setName(name);

    // set the max number of objectmon on the team.
    System.out.println("Enter the number of Objectmon for team: " + name);

    int numObjectmon = this.scanner.nextInt();

    team.setMaxSize(numObjectmon);
    System.out.println("Which configuration method would you prefer? (1: random / 2: manual)");

    int configChoice = this.scanner.nextInt();

    if (configChoice == 1) {
      this.buildRandomTeam(team);
    } else {
      this.buildTeam(team);
    }
  }

  /**
   * Configures the maximum number of turns for the battle, and also configures all data for both
   * teams. Team configuration may be either random or manual.
   */
  public void configureFight() {
    System.out.println("Enter the maximum number of turns for the battle:");

    int maxNumTurns = this.scanner.nextInt();

    // set maximum number of turns.
    this.setMaxRounds(maxNumTurns);
    // configure each team.
    this.configureTeam(this.getTeamA());
    this.configureTeam(this.getTeamB());

    // reviewing the configuration.
    System.out.println(
        "Would you like to review your configuration before beginning? (1: yes / 2: no)");
    int reviewChoice = this.scanner.nextInt();
    if (reviewChoice == 1) {
      System.out.println(this.toString());
    }

    // configuration may be discarded if desired.
    System.out.println("Are you happy with your configuration? (1: yes / 2: no)");
    int happyChoice = this.scanner.nextInt();
    if (happyChoice == 2) {
      this.clearTeams();
      this.configureFight();
    }
  }

  public void buildTeam(Team team) {
    // loop for configuring all objectmon in the current team.
    for (int n = 0; n < team.getMaxSize(); n++) {
      // name ...
      System.out.println(
          "For team " + team.getName() + ", enter a name for Objectmon number " + (n + 1));
      String objectmonName = this.scanner.next();
      // hp ...
      System.out.println("For team " + team.getName() + ", enter the HP of " + objectmonName);
      int objectmonHP = this.scanner.nextInt();
      // stamina ...
      System.out.println("For team " + team.getName() + ", enter the stamina of " + objectmonName);
      int objectmonStamina = this.scanner.nextInt();
      // weight ...
      System.out.println("For team " + team.getName() + ", enter the weight of " + objectmonName);
      int objectmonWeight = this.scanner.nextInt();
      // adding the objectmon with these given parameters.
      this.addToTeam(
          team, new Objectmon(objectmonName, objectmonHP, objectmonStamina, objectmonWeight));
    }
  }

  public void buildRandomTeam(Team team) {
    int numObjectmon = team.getMaxSize();
    for (int i = 0; i < numObjectmon; i++) {
      this.addToTeam(team, new Objectmon(ObjectmonNameGenerator.nextName()));
    }
  }

  /**
   * Adds the given Objectmon to a team.
   *
   * @param team the Team in which to add the Objectmon.
   * @param objectmon the Objectmon to be added to the Team.
   * @return true if the Team changed as a result of the call.
   */
  public boolean addToTeam(Team team, Objectmon objectmon) {
    return team.add(objectmon);
  }

  /**
   * Adds the given Objectmon to teamA.
   *
   * @param objectmon the Objectmon to be added to the Team.
   * @return true if the Team changed as a result of the call.
   */
  public boolean addToTeamA(Objectmon objectmon) {
    return this.addToTeam(this.getTeamA(), objectmon);
  }

  /**
   * Adds the given Objectmon to teamB.
   *
   * @param objectmon the Objectmon to be added to the Team.
   * @return true if the Team changed as a result of the call.
   */
  public boolean addToTeamB(Objectmon objectmon) {
    return this.addToTeam(this.getTeamB(), objectmon);
  }

  /**
   * Indicates whether or not team can fight. A Team can fight if there is at least one Objectmon on
   * the Team who has not fainted.
   *
   * @param team the Team to check.
   * @return True if team can fight, false otherwise.
   */
  public boolean canTeamFight(Team team) {
    return team.canFight();
  }

  /**
   * Indicates whether or not teamA can fight. A Team can fight if there is at least one Objectmon
   * on the Team who has not fainted.
   *
   * @return True if teamA can fight, false otherwise.
   */
  public boolean canTeamAFight() {
    return this.canTeamFight(this.getTeamA());
  }

  /**
   * Indicates whether or not teamB can fight. A Team can fight if there is at least one Objectmon
   * on the Team who has not fainted.
   *
   * @return True if teamB can fight, false otherwise.
   */
  public boolean canTeamBFight() {
    return this.canTeamFight(this.getTeamB());
  }

  /**
   * Indicates whether or not the fight has a winner. A fight has a winner if either of the two
   * Teams can no longer fight.
   *
   * @return True if there is a winner, false otherwise.
   */
  public boolean isWinner() {
    if (this.canTeamFight(this.getTeamA()) && this.canTeamFight(this.getTeamB())) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * String representation of the TrainingGym, which is a combination of the string representations
   * of the two Teams comprising the gym.
   *
   * @return The string representation of this TrainingGym.
   */
  public java.lang.String toString() {
    StringJoiner sj = new StringJoiner("\n", "\n", "\n");

    sj.add(this.toStringTeam(this.getTeamA()));
    sj.add(this.toStringTeam(this.getTeamB()));
    return sj.toString();
  }

  /**
   * String representation of a Team, which is a combination of the string representations of the
   * individual Objectmon comprising the Team.
   *
   * @param team The team to be represented as a string.
   * @return The string representation of this Team.
   */
  public java.lang.String toStringTeam(Team team) {
    StringJoiner sj = new StringJoiner("\n", "\n", "\n");
    sj.add(team.getName());

    // grabbing the string representation of each objectmon.
    for (Object objectmon : team) {
      sj.add(((Objectmon) objectmon).toString());
    }
    return sj.toString();
  }
}
