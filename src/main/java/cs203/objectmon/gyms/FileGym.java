package cs203.objectmon.gyms;

import cs203.battlearena.objectmon.Objectmon;
import cs203.objectmon.teams.FileTeam;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringJoiner;

public class FileGym extends ListGym {
  public FileGym() {
    super(new FileTeam<Objectmon>("teamA"), new FileTeam<Objectmon>("teamB"));
  }

  public FileGym(FileTeam<Objectmon> teamA, FileTeam<Objectmon> teamB) {
    super(teamA, teamB);
  }

  public void configureTeam(FileTeam team) {
    System.out.println("Would you like to load a team from disk? (1: yes / 2: no)");

    if (this.getScanner().nextInt() == 1) {
      System.out.println("Specify the path for team:");
      try {
        team.load(this.getScanner().nextLine());
      } catch (IOException e) {
        e.printStackTrace();
        this.configureTeam(team);
      }
    } else {
      super.configureTeam(team);
    }
  }

  public void saveRound() {
    PrintWriter out = null;

    try {
      out = new PrintWriter(new FileWriter("lastRound.gym"));

      // current round.
      out.println(this.getCurrentRound());

      // team A:
      // printing the name.
      out.println(this.getTeamA().getName());

      // printing the max size.
      out.println(this.getTeamA().getMaxSize());

      // making team A into a FileTeam.
      FileTeam<Objectmon> teamA = new FileTeam<Objectmon>();
      teamA.addAll(this.getTeamA());

      for (int i = 0; i < this.getTeamA().size(); i++) {
        // gathering data from each objectmon.
        StringJoiner sj = new StringJoiner(", ");
        Objectmon oman = teamA.get(i);
        sj.add(oman.getName());
        sj.add(new Integer(oman.getHP()).toString());
        sj.add(new Integer(oman.getStamina()).toString());
        sj.add(new Integer(oman.getWeight()).toString());

        // printing the objectmon data.
        out.println(sj.toString());
      }

      // team B:
      // printing the name.
      out.println(this.getTeamB().getName());

      // printing the max size.
      out.println(this.getTeamB().getMaxSize());

      // making team B into a FileTeam.
      FileTeam<Objectmon> teamB = new FileTeam<Objectmon>();
      teamB.addAll(this.getTeamB());

      for (int i = 0; i < this.getTeamB().size(); i++) {
        // gathering data from each objectmon.
        StringJoiner sj = new StringJoiner(", ");
        Objectmon oman = teamB.get(i);
        sj.add(oman.getName());
        sj.add(new Integer(oman.getHP()).toString());
        sj.add(new Integer(oman.getStamina()).toString());
        sj.add(new Integer(oman.getWeight()).toString());

        // printing the objectmon data.
        out.println(sj.toString());
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      // closing the file.
      if (out != null) {
        out.close();
      }
    }
  }

  @Override
  public void executeRound() {
    // execute the round as normal.
    super.executeRound();
    this.saveRound();
  }
}
