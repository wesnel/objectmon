package cs203.objectmon.gyms;

import cs203.battlearena.objectmon.ObjectdexEntry;
import cs203.battlearena.objectmon.Objectmon;
import cs203.battlearena.teams.Team;
import cs203.objectmon.objectdex.JsonObjectdex;
import cs203.objectmon.teams.FileTeam;
import java.io.IOException;
import java.util.ArrayList;

public class JsonGym extends FileGym {
  public JsonGym() {
    super();
  }

  public JsonGym(FileTeam<Objectmon> teamA, FileTeam<Objectmon> teamB) {
    super(teamA, teamB);
  }

  @Override
  public void configureTeam(Team team) {
    System.out.println(
        "Would you like to build this team from a saved JsonObjectdex? (1: yes / 2: no)");
    if (this.getScanner().nextInt() == 1) {
      System.out.println("Specify the path to the JsonObjectdex:");
      JsonObjectdex<String, ObjectdexEntry> jodex = new JsonObjectdex<String, ObjectdexEntry>();

      try {
        jodex.load(this.getScanner().nextLine());
      } catch (IOException e) {
        e.printStackTrace();
        this.configureTeam(team);
      }

      ArrayList<Objectmon> toAdd = new ArrayList<Objectmon>();

      for (ObjectdexEntry oEntry : jodex.values()) {
        toAdd.add(jodex.createObjectmon(oEntry.getName()));
      }

      team.setMaxSize(toAdd.size());
      team.addAll(toAdd);
    } else {
      super.configureTeam(team);
    }
  }
}
