package cs203.objectmon.gyms;

import cs203.battlearena.objectmon.Objectmon;
import cs203.objectmon.teams.ListTeam;

public class ListGym extends TrainingGym {
  public ListGym() {
    super(new ListTeam<Objectmon>("teamA"), new ListTeam<Objectmon>("teamB"));
  }

  public ListGym(ListTeam<Objectmon> teamA, ListTeam<Objectmon> teamB) {
    super(teamA, teamB);
  }
}
